package com.company;

public class Main {

    public static void main(String[] args) {
        //primitive types
	    //byte
        //short
        //int
        //long
        //float
        //double
        //char
        //boolean

        //String is a class
        //String is sequence of characters
        String myString = "This is a string";
        System.out.println("myString is equal to " + myString);
        myString = myString + ", and this is more.";
        System.out.println("myString is equal to " + myString);
        myString = myString + " \u00a9 2019";
        System.out.println("myString is equal to " + myString);

        //concatenating string even if number
        String numberString = "250.55";
        numberString = numberString + "49.95";
        System.out.println(numberString);

        String lastString = "10";
        int myInt = 50;
        lastString = lastString + myInt;
        //converts int to String
        System.out.println("lastString is equal to " + lastString);
        double doubleNumber = 120.47d;
        lastString = lastString + doubleNumber;
        //converts double to String
        System.out.println("lastString is equal to " + lastString);
    }
}
