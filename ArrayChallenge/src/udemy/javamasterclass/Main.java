package udemy.javamasterclass;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void getIntegers(int capacity) {
        int[] intArray = new int[capacity];

        System.out.println("Please enter " + capacity + " numbers");
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scanner.nextInt();
        }

        intArray = sortIntegers(intArray);
        printArray(intArray);
    }

    public static int[] sortIntegers(int[] array) {
        int[] newArray = Arrays.copyOf(array, array.length);
        boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            //new array - 1 because we're looking ahead
            for (int i = 0; i < newArray.length - 1; i++) {
                //if element is less than next element
                if (newArray[i] < newArray[i + 1]) {
                    //store current element
                    temp = newArray[i];
                    //set current element to next element
                    newArray[i]  = newArray[i + 1];
                    //reset next element to stored element
                    newArray[i + 1] = temp;
                    //continue loop
                    flag = true;
                }
            }
        }

        return newArray;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Number is " + array[i] + "\r");
        }
    }

    public static void main(String[] args) {
        // Create a program using arrays that sorts a list of integers in descending order.
        // Descending order is highest value to lowest.
        // In other words if the array had the values in it 106, 26, 81, 5, 15 your program should
        // ultimately have an array with 106,81,26, 15, 5 in it.
        // Set up the program so that the numbers to sort are read in from the keyboard.
        // Implement the following methods - getIntegers, printArray, and sortIntegers
        // getIntegers returns an array of entered integers from keyboard
        // printArray prints out the contents of the array
        // and sortIntegers should sort the array and return a new array containing the sorted numbers
        // you will have to figure out how to copy the array elements from the passed array into a new
        // array and sort them and return the new sorted array.

        getIntegers(5);
    }
}
