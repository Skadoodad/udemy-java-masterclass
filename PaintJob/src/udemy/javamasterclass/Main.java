package udemy.javamasterclass;

public class Main {

    public static void main(String[] args) {
        System.out.println(getBucketCount(0.75, 0.75, 0.5, 0));
        System.out.println(getBucketCount(2.25, 2.0, 0));
        System.out.println(getBucketCount(0.75, 0.75, 0.5, 0));
    }

    public static int getBucketCount (double width, double height, double areaPerBucket, int extraBuckets) {
        double buckets = 0;
        if (width > 0 && height > 0 && areaPerBucket > 0 && extraBuckets >= 0) {
            buckets = width * height / areaPerBucket;
            buckets = Math.ceil(buckets);
            return (int) (buckets - extraBuckets);
        }

        return -1;
    }

    public static int getBucketCount (double width, double height, double areaPerBucket) {
        double buckets = 0;
        if (width > 0 && height > 0 && areaPerBucket > 0) {
            buckets = width * height / areaPerBucket;
            buckets = Math.ceil(buckets);
            return (int) buckets;
        }

        return -1;
    }

    public static int getBucketCount (double area, double areaPerBucket) {
        double buckets = 0;
        if (area > 0 && areaPerBucket > 0) {
            buckets = area / areaPerBucket;
            buckets = Math.ceil(buckets);
            return (int) buckets;
        }

        return -1;
    }
}
