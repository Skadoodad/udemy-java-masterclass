package udemy.javamasterclass;

class Car {
    private String name;
    private boolean engine;
    private int cylinders;
    private int wheels;
    private String color;
    private int gasTank;

    public Car(String name, int cylinders, String color, int gasTank) {
        this.name = name;
        this.engine = true;
        this.cylinders = cylinders;
        this.wheels = 4;
        this.color = color;
        this.gasTank = gasTank;
    }

    public String getName() {
        return name;
    }

    public boolean isEngine() {
        return engine;
    }

    public int getCylinders() {
        return cylinders;
    }

    public int getWheels() {
        return wheels;
    }

    public String getColor() {
        return color;
    }

    public int getGasTank() {
        return gasTank;
    }

    public String startEngine() {
        return "The engine starts";
    }

    public String accelerate() {
        return "The car starts to move faster";
    }

    public String brake() {
        return "The car slows down";
    }
}

class Porsche extends Car {
    public Porsche(String name, int cylinders, String color, int gasTank) {
        super(name, cylinders, color, gasTank);
    }

    @Override
    public String startEngine() {
        return "The engine starts, revs, and purrs calmly";
    }

    @Override
    public String accelerate() {
        return "The Porsche jumps off the line and accelerates steadily with every shift";
    }

    @Override
    public String brake() {
        return "The Porsche slows quickly in a controlled manner";
    }
}

class Corvette extends Car {
    public Corvette(String name, int cylinders, String color, int gasTank) {
        super(name, cylinders, color, gasTank);
    }

    @Override
    public String startEngine() {
        return "The engine roars to life";
    }

    @Override
    public String accelerate() {
        return "The Corvette wheels screech and the engine howls as it goes from zero to super fast....fast";
    }

    @Override
    public String brake() {
        return "The wheels scream as the Corvette tries to stop";
    }
}

class Miata extends Car {
    public Miata(String name, int cylinders, String color, int gasTank) {
        super(name, cylinders, color, gasTank);
    }

    @Override
    public String startEngine() {
        return "I think the car started, it's not making any noise";
    }

    @Override
    public String accelerate() {
        return "It's going forward... I think, did it just get passed by a rascal?";
    }

    @Override
    public String brake() {
        return "This is the easy part, this car barely moves";
    }
}

public class Main {
    public static void main(String[] args) {
	    //We are going to go back to the car analogy.
        //Create a base class called Car
        //It should have a few fields that would be appropriate for generic car class.
        //engine, cylinders, wheels, etc.
        //Constructor should initialize cylinders (number of) and name, and set wheels to 4
        //and engine to true. Cylinders and names would be passed parameters.

        //Create appropriate getters

        //Create some methods like startEngine, accelerate, and brake

        //show a message for each in the base class
        //Now create 3 sub classes for your favorite vehicles.
        //Override the appropriate methods to demonstrate polymorphism in use.
        //put all classes in the one java file (this one).

        Car car = new Car("Base car", 4, "Teal", 15);
        Car porsche = new Porsche("Porsche", 6, "Red", 12);
        Car corvette = new Corvette("Corvette", 8, "Yellow", 15);
        Car miata = new Miata("Miata", 4, "Brown", 10);

        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());
        System.out.println();

        System.out.println(porsche.startEngine());
        System.out.println(porsche.accelerate());
        System.out.println(porsche.brake());
        System.out.println();

        System.out.println(corvette.startEngine());
        System.out.println(corvette.accelerate());
        System.out.println(corvette.brake());
        System.out.println();

        System.out.println(miata.startEngine());
        System.out.println(miata.accelerate());
        System.out.println(miata.brake());
        System.out.println();
    }
}
