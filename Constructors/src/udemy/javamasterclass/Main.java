package udemy.javamasterclass;

public class Main {

    public static void main(String[] args) {
        //Create a new class for a bank account
        //Create fields for the account number, balance, customer name, email and phone number.

        //Create getters and setters for each field
        //Create two additional methods
        //1. To allow the customer to deposit funds (this should increment the balance field).
        //2. To allow the customer to withdraw funds. This should deduct from the balance field,
        //but not allow the withdrawal to complete if there are insufficient funds.
        //You will want to create various code in the Main class (the one created by IntelliJ) to
        //confirm your code is working.
        //Add some System.out.println's in the two methods above as well.

//        BankAccount bobsAccount = new BankAccount();
//        BankAccount bobsAccount = new BankAccount("12345", 0.00, "Bob Brown",
//                "myemail@bob.com", "(087) 123-4567");
//        System.out.println(bobsAccount.getAccountNumber());
//        System.out.println(bobsAccount.getBalance());
//
//        bobsAccount.withdrawFunds(100);
//
//        bobsAccount.depositFunds(50);
//        bobsAccount.withdrawFunds(100);
//
//        bobsAccount.depositFunds(51);
//        bobsAccount.withdrawFunds(100);

        BankAccount timsAccount = new BankAccount("Tim", "tim@email.com", "12345");
        System.out.println(timsAccount.getAccountNumber() + " name " + timsAccount.getCustomerName());
        System.out.println("Current balance is " + timsAccount.getBalance());
        timsAccount.withdrawFunds(100.55);
        System.out.println("Current balance is " + timsAccount.getBalance());
        System.out.println();

        //Create a new class VipCustomer
        //it should have 3 fields name, credit limit, and email address.
        //create 3 constructors
        //1st constructor empty should call the constructor with 3 parameters with default values
        //2nd constructor should pass on the 2 values it receives and add a default value for the 3rd
        //3rd constructor should save all fields.
        //create getters only for this using code generation of intellij as setters won't be needed
        //test and confirm it works.

        VipCustomer customer1 = new VipCustomer();
        System.out.println(customer1.getName());

        VipCustomer customer2 = new VipCustomer("Bob", 25000.00);
        System.out.println(customer2.getName());

        VipCustomer customer3 = new VipCustomer("tim", 100.00, "tim@email.com");
        System.out.println(customer3.getName());
        System.out.println(customer3.getEmail());
    }
}
