package udemy.javamasterclass;

public class NumberToWords {
    public static void numberToWords (int number) {
        if (number >= 0) {
            int digits = getDigitCount(number);
            number = reverse(number);
            for (int i = 0; i < digits; i++) {
                switch (number % 10) {
                    case 0:
                        System.out.println("Zero");
                        break;
                    case 1:
                        System.out.println("One");
                        break;
                    case 2:
                        System.out.println("Two");
                        break;
                    case 3:
                        System.out.println("Three");
                        break;
                    case 4:
                        System.out.println("Four");
                        break;
                    case 5:
                        System.out.println("Five");
                        break;
                    case 6:
                        System.out.println("Six");
                        break;
                    case 7:
                        System.out.println("Seven");
                        break;
                    case 8:
                        System.out.println("Eight");
                        break;
                    case 9:
                        System.out.println("Nine");
                        break;
                }
                number /= 10;
            }
        }
        else {
            System.out.println("Invalid Value");
        }
    }

    public static int reverse (int number) {
        int numberTrim = number;
        int reverse = 0;
        while(numberTrim != 0) {
            reverse += numberTrim % 10;
            if (numberTrim / 10 != 0) {
                reverse *= 10;
            }
            numberTrim /= 10;
        }

        return reverse;
    }

    public static int getDigitCount (int number) {
        int digits = 0;
        if (number >= 0) {
            while (number >= 10) {
                digits++;
                number /= 10;
            }
            digits++;
            return digits;
        }
        return -1;
    }
}
