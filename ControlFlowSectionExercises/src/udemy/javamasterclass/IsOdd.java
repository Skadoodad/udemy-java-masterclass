package udemy.javamasterclass;

public class IsOdd {
    public static boolean isOdd(int number) {
        if(number > 0) {
            if(number % 2 != 0) {
                return true;
            }
        }
        return false;
    }
}
