package udemy.javamasterclass;

public class SumFirstAndLastDigit {
    public static int sumFirstAndLastDigit(int number) {
        int firstNumber = 0;
        int lastNumber = 0;
        if(number >= 10) {
            lastNumber = number % 10;
            while (number >= 10) {
                number /= 10;
            }
            firstNumber = number;
            return firstNumber + lastNumber;
        }
        else if (number >= 0){
            return number + number;
        }
        return -1;
    }
}
