package udemy.javamasterclass;

public class GetEvenDigitSum {
    public static int getEvenDigitSum(int number) {
        if (number >= 0) {
            int evenDigitSum = 0;
            while (number >= 10) {
                if ((number % 10) % 2 == 0) {
                    evenDigitSum += number % 10;
                }
                number /= 10;
            }
            if (number % 2 == 0) {
                evenDigitSum += number;
            }
            return evenDigitSum;
        }
        return -1;
    }
}
