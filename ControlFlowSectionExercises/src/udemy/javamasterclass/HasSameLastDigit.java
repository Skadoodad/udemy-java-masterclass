package udemy.javamasterclass;

public class HasSameLastDigit {
    public static boolean hasSameLastDigit(int firstNumber, int secondNumber, int thirdNumber) {
        if (isValid(firstNumber) && isValid(secondNumber) && isValid(thirdNumber)) {
            if (firstNumber % 10 == secondNumber % 10 || firstNumber % 10 == thirdNumber % 10) {
                return true;
            }
            if (secondNumber % 10 == thirdNumber % 10) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValid(int testNumber) {
        if (testNumber >= 10 && testNumber <= 1000) {
            return true;
        }
        return false;
    }
}
