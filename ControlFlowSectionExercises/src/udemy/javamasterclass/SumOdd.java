package udemy.javamasterclass;

public class SumOdd {
    public static int sumOdd(int start, int end) {
        if(start > 0 && end > 0 && start <= end) {
            int sum = 0;
            for(int i = start; i <= end; i++) {
                if(IsOdd.isOdd(i)) {
                    sum += i;
                }
            }
            return sum;
        }
        return -1;
    }
}
