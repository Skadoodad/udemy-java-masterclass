package udemy.javamasterclass;

import java.util.Scanner;

public class InputThenPrintSumAndAverage {
    public static void inputThenPrintSumAndAverage () {
        Scanner scanner = new Scanner(System.in);
        boolean hasNextInt;
        int nextInt = 0;
        int sum = 0;
        int count = 0;
        double average = 0;

        while (true) {
            hasNextInt = scanner.hasNextInt();

            if (hasNextInt) {
                nextInt = scanner.nextInt();
                sum += nextInt;
                count++;
                average = (double) sum / (double) count;
                average = Math.round(average);
            }
            else {
                break;
            }
            scanner.nextLine();
        }

        scanner.close();
        System.out.println("SUM = " + sum + " AVG = " + (int) average);
    }
}
