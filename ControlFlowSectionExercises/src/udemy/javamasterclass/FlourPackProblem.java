package udemy.javamasterclass;

public class FlourPackProblem {
    public static boolean canPack (int bigCount, int smallCount, int goal) {
        if (bigCount >= 0 && smallCount >= 0 && goal >= 0) {
            int kilos = 0;

            for (int i = 0; i <= bigCount; i++) {
                if (kilos < goal) {
                    for (int j = 0; j < smallCount; j++) {
                        kilos++;
                        if (kilos == goal) {
                            return true;
                        }
                    }
                    kilos -= smallCount;
                    if (bigCount > 0) {
                        kilos += 5;
                        if (kilos == goal) {
                            return true;
                        }
                    }
                }
                else {
                    return false;
                }
            }
        }
        return false;
    }
}
