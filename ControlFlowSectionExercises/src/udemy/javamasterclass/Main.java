package udemy.javamasterclass;


public class Main {

    public static void main(String[] args) {
        System.out.println("***Print Number In Word***");
        PrintNumberInWord.printNumberInWord(1);
        System.out.println("***Get Days In Month***");
        System.out.println(GetDaysInMonth.getDaysInMonth(1, -2020));
        System.out.println("***Sum Odd***");
        System.out.println(SumOdd.sumOdd(1, 100));
        System.out.println("***Palindrome***");
        System.out.println(IsPalindrome.isPalindrome(-1221));
        System.out.println("****Sum First And Last Digit****");
        System.out.println(SumFirstAndLastDigit.sumFirstAndLastDigit(10));
        System.out.println("****Even Digit Sum****");
        System.out.println(GetEvenDigitSum.getEvenDigitSum(2000));
        System.out.println("****Has Shared Digit****");
        System.out.println(HasSharedDigit.hasSharedDigit(12, 13));
        System.out.println("****Has Same Last Digit****");
        System.out.println(HasSameLastDigit.hasSameLastDigit(9, 99, 999));
        System.out.println("****Get Greatest Common Divisor****");
        System.out.println(GetGreatestCommonDivisor.getGreatestCommonDivisor(81, 153));
        System.out.println("****All Factors****");
        PrintFactors.printFactors(32);
        System.out.println("****Perfect Number****");
        System.out.println(IsPerfectNumber.isPerfectNumber(-5));
        System.out.println("****Number To Words****");
        NumberToWords.numberToWords(7584);
        System.out.println("****Flour Pack Problem****");
        System.out.println(FlourPackProblem.canPack(2, 2, 50));
        System.out.println("****Get Largest Prime****");
        System.out.println(GetLargestPrime.getLargestPrime(873));
        System.out.println("****Print Square Star****");
        PrintSquareStar.printSquareStar(23);
        System.out.println("****Input Then Print Sum And Average****");
        InputThenPrintSumAndAverage.inputThenPrintSumAndAverage();
    }
}
