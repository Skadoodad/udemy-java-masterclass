package udemy.javamasterclass;

public class GetGreatestCommonDivisor {
    public static int getGreatestCommonDivisor(int first, int second) {
        if (first >= 10 && second >= 10) {
            int maxFirst = first;
            int maxSecond = second;

            for (int i = maxFirst; i > 0; i--) {
                if (first % i == 0) {
                    maxFirst = i;
                    for (int j = maxSecond; j > 0; j--) {
                        if (second % j == 0) {
                            maxSecond = j;
                            if (maxFirst == maxSecond) {
                                return maxFirst;
                            }
                            if (maxSecond < maxFirst) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }
}

