package udemy.javamasterclass;

public class PrintSquareStar {
    public static void printSquareStar (int number) {
        if (number >= 5) {
            //number is number of rows to print
            //5 would be 5 columns (stars) and 5 rows (stars)
            //when row number equals column number (1, 1) (2, 2) (3, 3)

            for (int row = 1; row <= number; row++) {
                for (int column = 1; column <= number; column++) {
                    if (row == 1 || row == number || column == number
                            || column == 1 || column == row || column == number - row + 1) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
        else {
            System.out.println("Invalid Value");
        }
    }
}
