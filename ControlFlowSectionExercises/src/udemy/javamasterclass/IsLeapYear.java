package udemy.javamasterclass;

public class IsLeapYear {
    public static boolean isLeapYear(int year) {
        if(year > 1 && year < 9999) {
            if(year % 4 == 0) {
                //yes unless also divisible by 100
                if(year % 100 == 0 && year % 400 != 0) {
                    //no unless divisible by 400
                    return false;
                }
                return true;
            }
        }
        return false;
    }
}
