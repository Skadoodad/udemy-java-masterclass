package udemy.javamasterclass;

public class GetLargestPrime {
    public static int getLargestPrime (int number) {
        if (number > 1) {
            int largestprime = -1;

            //eliminate even numbers
            //split in half until it won't
            while (number % 2 == 0) {
                largestprime = 2;
                number /= 2;
            }

            //only odd numbers now, not 3
            //start at 3, while i * i is less than number, increment by 2 to stick with odds
            for (int i = 3; i * i <= number; i += 2) {
                //if number divides evenly with i set prime to i
                while (number % i == 0) {
                    largestprime = i;
                    //decrease number by dividing by i
                    number /= i;
                }
            }

            //if number is greater than 2 set
            if (number > 2) {
                largestprime = number;
            }
            return largestprime;
        }
        return -1;
    }
}
