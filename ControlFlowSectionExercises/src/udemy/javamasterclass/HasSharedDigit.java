package udemy.javamasterclass;

public class HasSharedDigit {
    public static boolean hasSharedDigit(int firstNumber, int secondNumber) {
        if (firstNumber >= 10 && firstNumber <= 99 && secondNumber >= 10 && secondNumber <= 99) {
            int firstTest = 0;
            int secondTest = 0;

            while (firstNumber != 0) {
                firstTest = firstNumber % 10;
                secondTest = secondNumber;
                while (secondTest != 0) {
                    if (firstTest % 10 == secondTest % 10) {
                        return true;
                    }
                    secondTest /= 10;
                }
                firstNumber /= 10;
            }
        }
        return false;
    }
}
