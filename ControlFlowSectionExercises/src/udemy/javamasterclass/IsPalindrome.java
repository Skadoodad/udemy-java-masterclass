package udemy.javamasterclass;

public class IsPalindrome {
    public static boolean isPalindrome(int number) {
        if(number < 0) {
            number *= -1;
        }
        int numberTrim = number;
        int reverse = 0;
        while(numberTrim >= 10) {
            reverse += numberTrim % 10;
            reverse *= 10;
            numberTrim /= 10;
        }
        reverse += numberTrim;
        if(number == reverse) {
            return true;
        }
        return false;
    }
}
