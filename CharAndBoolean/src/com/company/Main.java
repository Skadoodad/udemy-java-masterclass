package com.company;

public class Main {

    public static void main(String[] args) {

        // char is 2 bytes of memory
        // https://unicode-table.com
        char myChar = 'D';
        char myUnicodeChar = '\u0044'; //unicode D
        System.out.println(myChar);
        System.out.println(myUnicodeChar);

        char myCopyrightChar = '\u00a9'; //unicode copyright
        System.out.println(myCopyrightChar);

        boolean myTrueBooleanValue = true;
        boolean myFalseBooleanValue = false;

        boolean isCustomerOverTwentyOne = true;
    }
}
