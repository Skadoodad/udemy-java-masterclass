package udemy.javamasterclass;

import java.util.ArrayList;

public class MobilePhone {
    private String myNumber;
    private ArrayList<Contact> myContacts;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContacts = new ArrayList<Contact>();
    }

    public boolean addNewContact(Contact contact) {
        //check for name already ArrayList
        if (findContact(contact.getName()) >= 0) {
            System.out.println("Contact is already on file");
            return false;
        }

        //add name to end of ArrayList
        myContacts.add(contact);
        return true;
    }

    public boolean updateContact(Contact oldContact, Contact newContact) {
        //search for oldContact already existing in ArrayList
        int foundPosition = findContact((oldContact));
        //if search criteria is not found in list -1 is returned
        if (foundPosition < 0) {
            //stop the process here and give error warning
            System.out.println(oldContact.getName() + ", was not found.");
            return false;
        }
        else if (findContact(newContact.getName()) != -1) {
            System.out.println("Contact with name " + newContact.getName() +
            " already exists. Update was not successful.");
            return false;
        }

        //replace contact at found position with newContact, return true, valid message
        this.myContacts.set(foundPosition, newContact);
        System.out.println(oldContact.getName() + ", was replaced with " + newContact.getName());
        return true;
    }

    public boolean removeContact(Contact contact) {
        //search for contact in ArrayList
        int foundPosition = findContact(contact);
        //if not found -1 is returned
        if (foundPosition < 0) {
            //message that contact was not found return false
            System.out.println(contact.getName() + ", was not found.");
            return false;
        }

        //remove contact at position it was previously found at, valid message, return true
        this.myContacts.remove(foundPosition);
        System.out.println(contact.getName() + ", was deleted.");
        return true;
    }

    //mini method to find contacts in the ArrayList
    private int findContact(Contact contact) {
        return this.myContacts.indexOf(contact);
    }

    //method to find contact by name using loop
    private int findContact(String contactName) {
        for (int i = 0; i < myContacts.size(); i++) {
            Contact contact = this.myContacts.get(i);
            if (contact.getName().equals(contactName)) {
                return i;
            }
        }
        return -1;
    }

    //method to return contact name searching by contact
    public String queryContact(Contact contact) {
        if (findContact(contact) >= 0) {
            return contact.getName();
        }
        return null;
    }

    //method to return contact search by name
    public Contact queryContact(String name) {
        int position = findContact(name);
        if (position >= 0) {
            return this.myContacts.get(position);
        }

        return null;
    }

    //method to print full contact ArrayList
    public void printContacts() {
        System.out.println("Contact List");
        for (int i = 0; i < this.myContacts.size(); i++) {
            System.out.println((i + 1) + "." +
                this.myContacts.get(i).getName() + " -> " +
                this.myContacts.get(i).getPhoneNumber());
        }
    }
}
