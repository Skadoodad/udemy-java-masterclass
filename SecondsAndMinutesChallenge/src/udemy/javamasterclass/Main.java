package udemy.javamasterclass;

public class Main {
    private static final String INVALID_VALUE_MESSAGE = "Invalid value";

    public static void main(String[] args) {
        System.out.println(getDurationString(65, 45));
        System.out.println(getDurationString(3945));
        System.out.println(getDurationString(-41));
        System.out.println(getDurationString(65,9));
    }

    private static String getDurationString(int minutes, int seconds) {
        if(minutes < 0 || seconds < 0 || seconds > 59) {
            return INVALID_VALUE_MESSAGE;
        }
        return minutes / 60 + "h " + minutes % 60 + "m " + seconds + "s";
    }

    public static String getDurationString(int seconds) {
        if(seconds >= 0) {
            return getDurationString(seconds / 60, seconds % 60);
        }
        return INVALID_VALUE_MESSAGE;
    }
}
