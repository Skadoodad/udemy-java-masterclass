package udemy.javamasterclass;

public class Main {

    public static void main(String[] args) {
        //indenting is good but white space doesn't matter

        //statement is the whole line
        //expression is "myVariable = 50"
	    int myVariable = 50;

	    myVariable++;
	    myVariable--;
        System.out.println("This is a test");

        System.out.println("This is" +
                " another" +
                " still more.");

        //same line spacing doesn't matter
        int anotherVariable = 50;myVariable--;System.out.println("This is another one");
    }
}
