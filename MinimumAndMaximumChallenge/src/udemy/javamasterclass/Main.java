package udemy.javamasterclass;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    //Read the numbers from the console entered by the user and print the minimum and maximum number
        // the user has entered.
        //Before the user enters the number, print the message "Enter number:"
        //If the user enters an invalid number, break out of the loop and print the minimum and maximum number.

        Scanner scanner = new Scanner(System.in);
        int maxNumber = Integer.MIN_VALUE;
        int minNumber = Integer.MAX_VALUE;
        int number;
        boolean hasNextInt;

        while (true) {
            System.out.println("Enter number: ");

            hasNextInt = scanner.hasNextInt();

            if (hasNextInt) {
                number = scanner.nextInt();
                if (number > maxNumber) {
                    maxNumber = number;
                }
                if (number < minNumber) {
                    minNumber = number;
                }
            }
            else {
                break;
            }
            scanner.nextLine(); // handle enter as input
        }
        scanner.close();
        System.out.println("Max number: " + maxNumber);
        System.out.println("Min number: " + minNumber);
    }
}
