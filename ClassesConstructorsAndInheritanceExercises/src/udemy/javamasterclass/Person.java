package udemy.javamasterclass;

public class Person {
    private String firstName;
    private String lastName;
    private int age;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        if (age >= 1 && age <= 100) {
            this.age = age;
        }
        else {
            this.age = 0;
        }
    }

    public int getAge() {
        return age;
    }

    public boolean isTeen() {
        if (this.age < 20 && this.age > 12) {
            return true;
        }
        return false;
    }

    public String getFullName() {
        if (!this.firstName.equals("") && !this.lastName.equals("")) {
            return this.firstName + " " + this.lastName;
        }
        else if (this.lastName.equals("")) {
            return this.firstName;
        }
        else if (this.firstName.equals("")) {
            return this.lastName;
        }
        return "";
    }
}
