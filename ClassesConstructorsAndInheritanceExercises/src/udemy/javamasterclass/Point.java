package udemy.javamasterclass;

public class Point {
    private int x;
    private int y;

    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance() {
        double stuff = this.x * this.x + this.y * this.y;

        return Math.sqrt(stuff);
    }

    public double distance(int x, int y) {
        Point point = new Point(x, y);
        return distance(point);
    }

    public double distance(Point point) {
        double xStuff = point.getX() - this.x;
        xStuff *= xStuff;
        double yStuff = point.getY() - this.y;
        yStuff *= yStuff;

        return Math.sqrt(xStuff + yStuff);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
