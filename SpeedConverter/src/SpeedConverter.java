public class SpeedConverter {

    public static long toMilesPerHour(double kilometersPerHour) {
        if(kilometersPerHour < 0) {
            return -1;
        }

        return Math.round(kilometersPerHour / 1.609);
    }

    public static void printConversion(double kilometersPerHour) {
        if(kilometersPerHour < 0) {
            System.out.println("Invalid value");
        }
        else {
            long milesPerHour = toMilesPerHour(kilometersPerHour);
            System.out.println(kilometersPerHour + "km/h = " + milesPerHour + "mi/h");
        }
    }


    public static boolean isLeapYear(int year) {
        if(year <= 1 || year > 9999) {
            return false;
        }

        boolean leapYear = false;

        //step 1
        if(year % 4 == 0) {
            //step 2
            if(year % 100 == 0) {
                //step 3
                if(year % 400 == 0) {
                    leapYear = true;
                }
            }
        }
        //step 5
        return leapYear;
    }
}
