package udemy.javamasterclass;

public class Main {

    public static void main(String[] args) {

        int result = 1 + 2; //1 + 2 = 3
        System.out.println("1 + 2 = " + result);
        int previousResult = result;
        System.out.println("previousResult = " + previousResult);
        result = result - 1;
        System.out.println("3 - 1 = " + result);
        System.out.println("previousResult = " + previousResult);

        result = result * 10; //2 * 10 = 20
        System.out.println("2 * 10 = " + result);

        result = result / 5; //20 / 5 = 4
        System.out.println("20 / 5 = " + result);

        result = result % 3; //remainder of (4 % 3)
        System.out.println("20 % 3 = " + result);

        //result = result + 1
        result++;
        System.out.println("1 + 1 = " + result);

        //result + result - 1
        result--;
        System.out.println("2 - 1 = " + result);

        //result = result + 2
        result += 2;
        System.out.println("1 + 2 = " + result);

        //result = result * 10
        result *= 10;
        System.out.println("3 * 10 = " + result);

        //result = result / 3
        result /= 3;
        System.out.println("30 / 3 = " + result);

        //result = result - 2
        result -= 2;
        System.out.println("10 - 2 = " + result);

        boolean isAlien = false;
        if (isAlien == false) {
            System.out.println("It is not an alien!");
            System.out.println("And I am scare of aliens");
        }

        int topScore = 80;
        if (topScore < 100) {
            System.out.println("You got the high score!");
        }

        int secondTopScore = 95;
        if ((topScore > secondTopScore) && (topScore < 100)) {
            System.out.println("Greater than second top score and less than 100");
        }

        if ((topScore > 90) || (secondTopScore <= 90)) {
            System.out.println("Either or both of the conditions are true");
        }

        int newValue = 50;
        if (newValue == 50) {
            System.out.println("This is true");
        }

        boolean isCar = false;
        if(isCar) {
            System.out.println("This is not supposed to happen");
        }

        boolean wasCar = isCar ? true : false;
        if (wasCar) {
            System.out.println("wasCare is true");
        }

        double doubleOne = 20.00;
        double doubleTwo = 80.00;
        double doublesTotal = (doubleOne + doubleTwo) * 100.00d;
        System.out.println("MyValuestotal = " + doublesTotal);
        double doubleRemainder = doublesTotal % 40.00d;
        System.out.println("doubleRemainder = " + doubleRemainder);
        boolean isNoRemainder = (doubleRemainder == 0) ? true : false;
        System.out.println("isNoRemainder = " + isNoRemainder);
        if (!isNoRemainder) {
            System.out.println("Got some remainder 1");
        }

        boolean remainder = (doubleOne + doubleTwo) * 100 % 40.00 == 0 ? true : false;
        System.out.println(remainder);
        if (!remainder) {
            System.out.println("Got some remainder 2");
        }
    }
}
