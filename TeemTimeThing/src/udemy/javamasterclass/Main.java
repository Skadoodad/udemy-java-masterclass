package udemy.javamasterclass;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class Solution {
    public static void main(String[] args) {
        System.out.println(GetTimes());

        String reservationFifteen = "{" +
                "\"title\": \"My really awesome reservation\"," +
                "\"description\": \"\", " +
                "\"start\": \"2020-03-10T17:15:00+00:00\"," +
                "\"end\": \"2020-03-10T17:15:00+00:00\"" +
            "}";
        System.out.println(GetTimesReservation(reservationFifteen));
    }

//    Phase 1:
//    Create your function so that when taking no arguments or other input will return two objects, one object with the times
//    needed for now until the next hour or half hour mark, and the second with for now until the next hour or half hour mark following the first object.
//    The format should be a list of objects (maximum of two objects). Each object should have four keys, "value" (integer), "units"
//            (string), "start" (string, date in iso format), "end" (string, date in iso format).
//            [
//    {
//        "value": 14,
//            "units": "mins",
//            "start": "2020-03-10T17:35:01.736315+00:00",
//            "end": "2020-03-10T18:00:00+00:00"
//    },
//    {
//        "value": 44,
//            "units": "mins",
//            "start": "2020-03-10T17:35:01.736315+00:00",
//            "end": "2020-03-10T18:30:00+00:00"
//    }
//]
    public static String GetTimes() {
        String jsonReturn = "";
        LocalDateTime now = LocalDateTime.now();
        int minute = now.getMinute();
        boolean underThirty = minute < 30;

        int i = underThirty ? (30 - minute) : (60 - minute);
        int j = underThirty ? (60 - minute) : (90 - minute);

        jsonReturn +=
                "[{\n" +
                    "\"value\": " + i + ", \n" +
                    "\"units\": " + "\"mins\",\n" +
                    "\"start\": \"" + now + "\",\n" +
                    "\"end\": \"" + now.plusMinutes(i) + ",\n" +
                    "},\n" +
                    "{\n" +
                    "\"value\": " + j + ", \n" +
                    "\"units\": " + "\"mins\",\n" +
                    "\"start\": \"" + now + "\",\n" +
                    "\"end\": \"" + now.plusMinutes(j) + "\"\n" +
                "}]";
        return jsonReturn;
    }

//    Phase 2:
//    Modify your function to take into account a calendar reservation that will start at either the first or third quarter of the hour
//            (15 minute or 45 minute). The modified function should return the values that still meet the constraints described above.
//    Example calendar input object (description is optional, all others required):
//    {
//        "title": "My really awesome reservation",
//            "description": "",
//            "start": "2020-03-10T17:15:00+00:00",
//            "end": "2020-03-10T17:45:00+00:00"
//    }
    public static String GetTimesReservation(String reservation) {
        String jsonReturn = "";
        LocalDateTime now = LocalDateTime.now();
        Pattern pattern = Pattern.compile("(\"start\":\\s\"\\d{4}-\\d{2}-\\d{2}T\\d{2}:)(15|45)");
        Matcher matcher = pattern.matcher(reservation);
        int minute = 0;
        while (matcher.find()) {
            minute = parseInt(matcher.group(2));
        }

        boolean underThirty = minute < 30;

        int i = underThirty ? (30 - minute) : (60 - minute);
        int j = underThirty ? (60 - minute) : (90 - minute);

        jsonReturn +=
                "[{" +
                        "\"value\": " + i + ", " +
                        "\"units\": " + "\"mins\"," +
                        "\"start\": \"" + now + "\"," +
                        "\"end\": \"" + now.plusMinutes(i) + "," +
                        "}," +
                        "{" +
                        "\"value\": " + j + ", " +
                        "\"units\": " + "\"mins\"," +
                        "\"start\": \"" + now + "\"," +
                        "\"end\": \"" + now.plusMinutes(j) + "\"" +
                        "}]";
        return jsonReturn;
    }
}

/*
Your previous Plain Text content is preserved below:

At Teem one of our primary objectives is to help people find and use space quickly and efficiently. One of the ways that
we do that is through the EventBoard room display, and the feature of remainder and adjacent. The core of this function is being able to determine the available immediate use times of the room based on the calendar information that we have. This feature tries to fill time slots to the top and bottom of the hour, around other meetings that are scheduled.

Design a function that will calculate the remaining time from now to the bottom and top of the next hour, and return up
to two values. These values will be used by the view to produce something like the screenshot above. The end time of the last object should not be more than one hour in the future.

Phase 3:
Modify your function further to take into account a meeting that is currently in progress, and will end on the next hour
or half hour mark (whichever comes first). The modified function should return values that still meet the constraints of Phase 1.
Please note, that it is possible here to only return a list of one object.

Phase 4:
Given this function discuss the following items:
* Please describe a list of test cases for this function and its uses.
* Given that this function will need to be used by multiple products in a service environment, describe how to best architect
the solution.
*/