package udemy.javamasterclass;

public class Main {

    public static void main(String[] args) {

        //Challenge.
        //Start with a base class of vehicle, then create a Car class that inherits from this base class.
        //Finally, create another class, a specific type of Car that inherits from the Car class.
        //You should be able to handle steering, changing gears, and moving (speed in other words).
        //You will want to decide where to put the appropriate state and behaviors (fields and methods).
        //As mentioned above, changing gears, increasing/decreasing speed should be included.
        //For your specific type of vehicle you will want to add something specific for that type of car.

        Subaru subaru = new Subaru(36);
        subaru.steer(45);
        subaru.accelerate(30);
        subaru.accelerate(20);
        subaru.accelerate(-42);
    }
}
